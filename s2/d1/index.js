//Without the use of objects,  our students from s1 would be organized as follows if we record additional information

// let studentOneName = "John"
// let studentOneEmail = "john@mail.com"
// let studentOneGrades = [89, 84, 78, 88]

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.email} has logged in`)
  },
  logout() {
    console.log(`${this.email} has logged out`)
  },
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
  }
}

let studentTwoName = "Joe"
let studentTwoEmail = "joe@mail.com"
let studentTwoGrades = [78, 82, 79, 85]

let studentThreeName = "Jane"
let studentThreeEmail = "jane@mail.com"
let studentThreeGrades = [87, 89, 91, 93]

let studentFourName = "Jessie"
let studentFourEmail = "jessie@mail.com"
let studentFourGrades = [91, 89, 92, 93]

//actions that students may perform will be lumped together
const login = (email) => {
  console.log(`${email} has logged in`)
}

const logout = (email) => {
  console.log(`${email} has logged out`)
}

const listGrades = (grades) => {
  grades.forEach(grade => {
    console.log(grade)
  })
}

//this way of organizing students is not well organized
//this will become unmanageable when we add more students or functions
//to remedy this, we will create objects
