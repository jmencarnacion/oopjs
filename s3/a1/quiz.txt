What is the typical naming convention when creating a class?
Name
uppercase letter and must be singular

Should class methods be included in the class constructor?
outside of constructor

Can class methods be separated by commas?
not necessarily

Can we update an object’s properties via dot notation?
yes

What does a method need to return in order for it to be chainable?
return this.object