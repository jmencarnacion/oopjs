//Naming convention for class names should begin in an uppercase character and generally should be the sinuglar form of a word
class Student {
  //a constructor method defines HOW an object's structure is instantiated from the class
  constructor (name, email, grades) {
    this.name = name
    this.email = email
    this.gradeAve = undefined

    if (grades.length === 4) {
      if (grades.every(grade => typeof grade === "number")) {
        if (grades.every(grade => grade >= 0 && grade <= 100)) {
          this.grades = grades
        }else {
          this.grades = undefined
        }
      }else{
        this.grades = undefined
      }
    } else {
      this.grades = undefined
    }
  }

  //when adding methods to a class, make sure that they are outside the constructor method
  login() {
    console.log(`${this.email} has logged in`)
    return this
  }
  logout() {
    console.log(`${this.email} has logged out`)
    return this
  }
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    return this
  }

  computeAve() {
    let sum = 0
    this.grades.forEach(grade => sum = sum + grade)
    //when called, computeAve will assign the computed value to each object's gradeAve property
    this.gradeAve = sum/4
    return this
  }

}

let studentOne = new Student ("John", "john@mail.com", [89,84,78,88])
let studentTwo = new Student ("Joe", "joe@mail.com", [78, 82, 79, 85])
let studentThree = new Student ("Jane", "jane@mail.com", [87, 89, 91, 93])
let studentFour = new Student ("Jessie", "jessie@mail.com", [91, 89, 92, 93])

console.log(studentOne)
console.log(studentTwo)
console.log(studentThree)
console.log(studentFour)