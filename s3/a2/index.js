//Modify the Student class to have a new constructor property named "passed" and another new constructor property named "willPassWithHonors." Both properties should be given a value of undefined by default.

//Modify our willPass() and willPassWithHonors() methods from the previous session to be both chainable, and assign the values they return to the new "passed" and "willPassWithHonors" properties in our constructor

//Modify the Student class to have a new constructor property named "passed" and another new constructor property named "passedWithHonors." Both properties should be given a value of undefined by default.

//Modify our willPass() and willPassWithHonors() methods from the previous session to be both chainable, and assign the values they return to the new "passed" and "passedWithHonors" properties in our constructor

class Student {
  constructor(name, email, grades) {
    this.name = name
    this.email = email
    this.gradeAve = undefined
    this.passed = undefined
    this.passedWithHonors = undefined

    if (grades.length === 4) {
      if (grades.every(grade => typeof grade === "number")) {
        if (grades.every(grade => grade >= 0 && grade <= 100)) {
          this.grades = grades
        } else {
          this.grades = undefined
        }
      } else {
        this.grades = undefined
      }
    } else {
      this.grades = undefined
    }
  }

  login() {
    console.log(`${this.email} has logged in`)
    return this
  }
  logout() {
    console.log(`${this.email} has logged out`)
    return this
  }
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    return this
  }

  computeAve() {
    let sum = 0
    this.grades.forEach(grade => sum = sum + grade)
    this.gradeAve = sum / 4
    return this
  }

  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false
    return this
  }

  willPassWithHonors() {
    if (this.computeAve().gradeAve >= 90) {
      this.passedWithHonors = true
    } else if (this.computeAve() < 90 && this.computeAve() >= 85) {
      this.passedWithHonors = false
    } else {
      this.passedWithHonors = undefined
    }
    return this
  }

}