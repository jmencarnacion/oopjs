//ANSWERS BELOW ARE FROM DISCUSSION
//1//
let students = ["John", "Joe", "Jane", "Jessie"]

let addToEnd = (arr, element) => {
  if (typeof element !== "string") {
    return "Error - can only add strings to the array."
  }else{
    arr.push(element)
    console.log(arr)
  }
}
//test cases 
addToEnd(students, "Ryan")
addToEnd(students, 045)

//2//
let students = ["John", "Joe", "Jane", "Jessie"]

let addToEnd = (arr, element) => {
  if (typeof element !== "string") {
    return "Error - can only add strings to the array."
  } else {
    arr.unshift(element)
    console.log(arr)
  }
}
//test cases 
addToEnd(students, "Ryan")
addToEnd(students, 045)

//3//
let elementChecker = (arr,elementToCheck) => {
  if (arr.length === 0){
    console.log("Error - Passed in array is empty")
  }else{
    let result = arr.some(element => element === elementToCheck)
    console.log(result)
  }
}
//test cases
elementChecker(students, "Jane")
elementChecker([], "Jane")

//4//
let checkAllStringsEnding = (arr, char) => {
  if (arr.length === 0) {
    console.log("Error - passed in array is empty")
  } else if (arr.some(element => typeof element !== "string")) {
    console.log("Error - all array elements must be strings")
  } else if (char.length > 1) {
    console.log("Error - second argument must be a single character")
  } else {
    let result = arr.every (element => element[element.length-1] === char)
    console.log(result)
  }

}

//5//
let stringLengthSorter = (arr) => {
  if (arr.some(element => typeof element !== "string")) {
    console.log("Error - all array elements must be strings")
  }else{
    arr.sort((elementA, elementB) => {
      return elementA.length - elementB.length
    })
    console.log(arr)
  }
}

//6//
let startsWithCounter = (arr, char) => {
  if (arr.length === 0) {
    console.log("Error - passed in array is empty")
  } else if (arr.some(element => typeof element !== "string")) {
    console.log("Error - all array elements must be strings")
  } else if (char.length > 1) {
    console.log("Error - second argument must be a single character")
  } else {
    let result = 0;
    arr.forEach(element =>{
      (element[0].toLowerCase() === char.toLowerCase()) {
        result++
    }}
    console.log(result)
  }
}
//test
startsWithCounter(students,)

//7
let likeFinder = (arr, str) => {
  if (arr.length === 0) {
    console.log("Error - passed in array is empty")
  } else if (arr.some(element => typeof element !== "string")) {
    console.log("Error - all array elements must be strings")
  } else if (typeof str !== "string") {
    console.log("Error - second argument must be of data type string")
  } else {
    let result = []

    arr.forEach(element => {
      if(element.toLowerCase().includes(str.toLowerCase())){
        result.push(element)
      }
    })
    console.log(result)
  }
}

//8
let randomPicker = (arr) => {
  if (arr.length === 0) {
    console.log("Error - passed in array is empty")
  } else {
    console.log(arr[Math.floor(Math.random()*arr.length)])
  }
}
//test
randomPicker[students]