//Arrays: are a kind of predefined JS Objects
//Because arrays have a constructor, a new array object must first be created before it can be used
//Once a new Array Object is created, JavaScript's prdefeined array methods can be used

let students = ["John", "Joe", "Jane", "Jessie"]

//Math: is another kind of predefined JS Object
//However, unlike arrays, the Math Object has NO constructor. All of its properties and methods can be used without creating a Math object bneforehand. Because of this, the Math object is said to be "static".

// console.log(Math.PI)
/* 
Other predefined Math properties:

Math.E = return's Euler's number
Math.PI = returns PI
Math.SQRT2 = returns the square root of 2 (or any number)
Math.SQRT1_2 = returns the square root of 1/2 (or any fraction)
Math.LN2 = returns the natural logarithm of 2 (or any number)
Math.LOG2E = returns the base 2 logarithm of E (or any other base)
*/

//Math methods:
// console.log(Math.round(3.14)) //round to the nearest integer
// console.log(Math.ceil(3.14)) //round UP to the nearest integer
// console.log(Math.floor(3.14)) //round DOWN to the nearest integer
// console.log(Math.trunc(3.14)) //returns only the integer (no decimal)

// console.log(Math.min(-3,-2,-1,0,1,2,3)) //finds the lowest value in a list of arguments
// console.log(Math.max(-3,-2,-1,0,1,2,3)) //finds the lowest value in a list of arguments
// console.log(Math.random())//returns a random number between 0 (inclusive) to 1 (exclusive)

//Get a random number between 0 and 10
console.log(Math.floor(Math.random() * 99999))

/* //common JS primitives
string
number
boolean
undefined
null

*/